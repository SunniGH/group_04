# Group_04

Advanced Programming Groupwork

## Project Ergo

This repository contains the work developed by team members during the two-day hackathon held to study the energy mix of multiple countries. By analyzing the energy mix of multiple countries, we hope to contribute to the green transition. The energy data used is from Our World in Data and includes data on energy consumption (primary energy, per capita, and growth rates), energy mix, electricity mix and other relevant metrics from date 1900 to date 2019. Our analysis and key findings can be found in the showcase notebook.

In the first day of the hackathon we analyzed the energy consumption over time starting from 1970 in comparison with gdp and population development. Furthermore a country's energy mix is analyzed relatively and totally to detect patterns or major changes over time and across countries. 
During the second day we have deepened our analyze and added more relevant features such as  emissions to the dataset in order to draw better conclusions. Our analysis is supported by various graphical representations of the data. 

## Code Style

PEP8 compliant

## Authors 
- Sunniva Hermann - 48647
- Flavia Sironi - 48170
- Marie Vandeurzen - 49602
- Hannah Petry - 48458

## License
This project is licensed under the GNU General Public License v3.0.
([GNU](https://choosealicense.com/licenses/gpl-3.0/))


"""
This project Ergo is dedicated to the analysis of energy data.

Class:
    EnergyErgo: A class to analyze energy data from 1970 to 2021. It contains
    different methods to analyze and visualise the emissions, consumption
    and GDP from different countries
"""
import os
from typing import Union
import warnings
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from statsmodels.tsa.arima.model import ARIMA
warnings.filterwarnings("ignore")

class EnergyErgo:
    """
    A class to analyze the energy and consumption and mix of countries
    over the years since 1970.

    Attributes:
        data (pd.DataFrame): The energy dataset loaded into a pandas DataFrame.
        url (str): The link from where the energy dataset is downloaded.
        name (str): The name that is given to the downloaded energy dataset.

    Methods:
        get_data()
            This method downloads a csv file into a downloads directory from
            the given url and transforms it into a Pandas DataFrame,
            if the file is already downloaded,
            it gets data from downloads/energy.csv.

        list_countries()
            This method lists all available countries in the dataset.

        gapminder(year)
            This method plots the total energy consumption depending
            on the gdp in a specific year.

        consumption_emissions_compare(countries)
            This method creates a barplot that compares the total consumptions
            and total emissions accumulated over the years for the given
            countries.

        consumption_emissions_over_years(countries)
            This method creates a line chart that compares the total
            consumptions and total emissions over the years (1970-2020)
            for the given countries.

        gdp_compare(countries)
            This method creates a plot that compares the GDP
            for given countries.

        population_compare()
            This method creates three line charts that compare the population
            size of the three selected countries over the years.

        area_chart(country, normalize)
            This method creates an area chart of the "_consumption" columns
            of the energy dataset for a chosen country
            with the option to normalize.

        plot_prediction(country, prediction_periods)
            This method creates two plots that show the prediction for the
            emissions and consumption, for a given country and a
            given prediction period.

        emissions_consumption_scatter()
            This method creates a scatter plot between emissions and consumption
            for all countries, with the size of the dots should
            being the population.
    """

    def __init__(self, url : str =
"https://raw.githubusercontent.com/owid/energy-data/master/owid-energy-data.csv"
                 , name : str = 'energy.csv') -> None:
        """
        Initialize the variables needed for the rest of the class methods
        """

        self.url = url
        self.name = name

    def get_data(self) -> None:
        """
        Downloads a csv file into a downloads directory from the given url and
        transforms it into a Pandas DataFrame, if the file is already
        downloaded, it gets data from downloads/energy.csv.
        """

        save_path = os.getcwd() + "/downloads"
        save_path_files = os.listdir(save_path)
        file_name = self.name
        file_url = self.url
        df_getdata = pd.DataFrame()

        if file_name not in save_path_files:
            df_getdata = pd.read_csv(file_url)
            df_getdata.to_csv(f"{save_path}/{file_name}", index=False)

        else:
            df_getdata = pd.read_csv(f"{save_path}/{file_name}")

        df_getdata = df_getdata[df_getdata["year"] >= 1970]
        df_getdata["year"]= pd.to_datetime(df_getdata["year"],format="%Y")
        df_getdata["year"] = pd.DatetimeIndex(df_getdata["year"]).year
        df_getdata = df_getdata.set_index("year")
        df_getdata = df_getdata.drop(columns = ["fossil_fuel_consumption",
        "low_carbon_consumption", "renewables_consumption"
                                ,"primary_energy_consumption"])
        energy_source = ["biofuel_consumption", "coal_consumption",
                         "gas_consumption", "hydro_consumption",
                         "nuclear_consumption", "oil_consumption",
                         "solar_consumption", "wind_consumption"]
        energy_emission = [1450, 1000, 455, 90, 5.5, 1200, 53, 14]


        df_getdata["Emissions"] = df_getdata[energy_source].mul(energy_emission).sum(1)
        df_getdata["Emissions"] = df_getdata["Emissions"]*1e9/1e6

        self.data = df_getdata

    def list_countries(self) -> list:
        """
        Lists all available countries in the dataset

        Returns:
            countries (list): List of all  available countries in dataset.
        """
        df_energy = self.data
        countries = list(df_energy.country.unique())
        return countries

    def gapminder(self, year:int) -> None:
        """
        This method plots the total energy consumption depending on
        the gdp in a specific year.

        Parameter
        ---------
        year: int
            Input of user. The year to be chosen must be an integer,
            otherwise a TypeError is raised.

        Raises
        ------
        TypeError:
            If year is not of type integer.

        Returns
        -------
        plt.show():
            Returns a scatter plot of the gdp and total energy consumption
            for a specific year with the area of each dot corresponding
            to the population size.
        """
        if not isinstance(year, int):
            raise TypeError("Year not of type integer! Please try again ...")

        df_getdata = self.data
        df_year= df_getdata.loc[[year]]
        consumption_cols = [col for col in df_year.columns if '_consumption' in col]
        df_year = df_year.copy()
        df_year['total_consumption'] = df_year[consumption_cols].sum(axis=1)
        df_year = df_year.loc[df_year['total_consumption'] != 0].dropna(subset=
                                                                ['gdp','total_consumption'])
        df_year = df_year[df_year.country != 'World']

        # use the scatterplot function to build the bubble map
        plt.figure(figsize = (15,10))
        sns.scatterplot(data=df_year, x="gdp", y="total_consumption",
                            size="population",hue="country",
                            legend=False, sizes=(20, 2000))
        plt.title('Gapminder Energy Chart')
        plt.ylabel('Consumption in TWh')
        plt.xlabel('Total GDP')
        plt.ticklabel_format(style='plain', axis='x')
        plt.show()

    def consumption_emissions_compare(self, countries:Union[str,list]) -> None:
        """
        Creates a barplot that compares the total consumptions and total emissions accumulated over
        the years for the given countries. Thus, a comparison can be made between the selected
        countries and their total consumption and emissions over the years.

        Parameters:
            countries (str/list): The country or countries to plot the emissions and consumptions
            for.

        Raises:
            ValueError: If one of the countries given as a parameter in countries is not in the
            dataset.
        """

        energy_df = self.data

        consumption_cols = [col for col in energy_df.columns if "_consumption" in col]
        energy_df["total_consumption"] = energy_df[consumption_cols].sum(axis=1)

        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8,5), sharex=True)
        fig.suptitle("\nComparison of total Consumption & total Emissions", fontsize=14)
        fig.tight_layout(pad=4.0)

        if isinstance(countries, str):
            countries = [countries]

        #Plot Consumption
        list_country = []
        consumption = []

        for country in countries:
            if country not in self.list_countries():
                raise ValueError(f"""Country "{country}" is not in the dataset -
                                 please only use countries that are available. Please try again!""")

            list_country.append(country)
            consumption.append(energy_df.loc[energy_df.country==country]
                                   ["total_consumption"].sum())

        df_consumption = pd.DataFrame(columns=("Country","Total_Consumption"))
        df_consumption["Country"] = list_country
        df_consumption["Total_Consumption"] = consumption

        ax1.bar(df_consumption["Country"], df_consumption["Total_Consumption"])
        ax1.set_title("Total Consumption")


        #Plot Emissions
        emissions = []

        for country in countries:
            emissions.append(energy_df.loc[energy_df.country==country]["Emissions"].sum())

        df_consumption = pd.DataFrame(columns=("Country", "Total_Emissions"))
        df_consumption["Country"] = list_country
        df_consumption["Total_Emissions"] = emissions

        ax2.bar(df_consumption["Country"], df_consumption["Total_Emissions"])
        ax2.set_title("Total Emissions")

        plt.show()


    def consumption_emissions_over_years(self, countries: Union[str, list]) -> None:
        """
        Creates a line chart that compares the total consumptions and total emissions over
        the years (1970-2020) for the given countries.

        Parameters:
            countries (str/list): The country or countries to plot the emissions and consumptions
            for.

        Raises:
            ValueError: If one of the countries given as a parameter in countries is not in the
            dataset.
        """

        energy_df = self.data

        if isinstance(countries, str):
            countries = [countries]

        #Creating Total Consumption Column
        consumption_cols = [col for col in energy_df.columns if "_consumption" in col]

        #Two different y-axis on the sample plot
        fig,ax1 = plt.subplots()
        ax2 = ax1.twinx()

        #Dropping the years 2020 and 2021 because there is no data
        energy_df = energy_df.drop(2020)
        energy_df = energy_df.drop(2021)

        #Evaluate each element in the list individually
        for country in countries:
            if country not in self.list_countries():
                raise ValueError(f"""Country "{country}" is not in the dataset -
                                 please only use countries that are available. Please try again!""")

            df_comparison = energy_df.loc[energy_df.country==country]
            df_comparison = df_comparison.copy()
            df_comparison["total_consumption"] = df_comparison[consumption_cols].sum(axis=1)
            #sum of consumption columns for each year for specific country

            ax1.plot(df_comparison.index, df_comparison.total_consumption,
                        label = country+" Consumption")
            ax2.plot(df_comparison.index, df_comparison.Emissions, linestyle="dashed",
                         label = country+" Emissions")

        ax1.set_xlabel("Year",fontsize=12)
        ax1.set_ylabel("Consumption",fontsize=12)
        ax2.set_ylabel("Emissions",fontsize=12)
        lines, labels = ax1.get_legend_handles_labels()
        lines2, labels2 = ax2.get_legend_handles_labels()
        ax2.legend(lines + lines2, labels + labels2, loc="center left", bbox_to_anchor=(1.1, 0.5))
        ax1.set_title("\nComparison Consumptions & Emissions over the years\n", fontsize=14)
        plt.rcParams["figure.figsize"] = (10,5)

        plt.show()

    def gdp_compare(self, countries: Union[str, list]) -> None:
        """
        Creates a plot that compares the GDP for given countries.

        Parameters:
            countries (str/list): The country or countries to plot the GDP for.

        Raises:
            ValueError: If one of the countries given as a parameter in
            countries is not in the dataset.
        """
        energy_df = self.data

        if isinstance(countries, str):
            countries = [countries]

        years = energy_df.index.unique()
        compare_df = pd.DataFrame(index=years)

        for country in countries:
            if country not in self.list_countries():
                raise ValueError(f"Country '{country}' not in dataset.")
            country_df = energy_df[energy_df["country"] == country]
            compare_df = compare_df.join(country_df[["gdp"]], how="left")
            compare_df.rename({"gdp": country.title()}, axis=1, inplace=True)
            compare_df.replace({0: np.NaN}, inplace=True)

        compare_df.plot()
        plt.title("GDP per Country")
        plt.xlabel("Year")
        plt.ylabel("GDP (in USD)")
        plt.legend()
        plt.show()

    def population_compare(self) -> None:
        """
        Creates three line charts that compare the population size of the three selected countries
        over the years.
        """

        energy_df = self.data

        fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(6,8), sharex=True)
        fig.suptitle("\nDevelopment of Population Size over the Years", fontsize=14)

        df_germany = energy_df.loc[energy_df.country=="Germany"]
        ax1.plot(df_germany.index, df_germany.population)
        ax1.set_title("Germany")
        ax1.set_ylabel("million")

        df_india = energy_df.loc[energy_df.country=="India"]
        ax2.plot(df_india.index, df_india.population)
        ax2.set_title("India")
        ax2.set_ylabel("billion")

        df_uk = energy_df.loc[energy_df.country=="United Kingdom"]
        ax3.plot(df_uk.index, df_uk.population, label = "United Kingdom Population")
        ax3.set_title("United Kingdom")
        ax3.set_ylabel("million")

        plt.show()

    def area_chart(self, country: str, normalized: bool) -> None:
        """
        Creates an area chart of the consumption columns of the energy dataset.

        Parameters:
            country (str): The country to plot the consumption distribution for.
            normalized (bool): If "True" is selected, the data will be
                normalized, otherwise if "False" is selected, data won't be
                normalized in the graph.

        Raises:
            KeyError: If the country given as a parameter in country
                is not in the dataset.
        """
        df_country = self.data[self.data["country"] == country]

        if country not in list(np.unique(df_country["country"])):
            raise KeyError("Country is not in the list!")

        consumption_columns = [col for col in df_country.columns
                               if "_consumption" in col]

        if normalized is True:
            df_country["tot_consumption"] = df_country[
                consumption_columns].sum(axis=1)
            for col in consumption_columns:
                df_country[col] = df_country[col] / df_country[
                    "tot_consumption"]

        labels = [df_country[: df_country.index("_consumption")].title(
        ).replace("_", " ").strip() for df_country in consumption_columns]
        plt.stackplot(df_country.index,
                      df_country[consumption_columns].T, labels=labels)
        plt.title("Distribution of Energy Consumption for " + country)
        plt.xlabel("Year")
        plt.ylabel("Consumption")
        plt.legend(loc="upper left", bbox_to_anchor=(1, 1))
        plt.show()

    def plot_prediction(self, country:str, prediction_periods:int) -> None:
        """
        Creates two plots that show the prediction for the emissions and
                consumption, for a given country and a given prediction period.

        Parameters:
            country (str): The country to plot the predicion for.
            prediction_periods (int): The amount of periods that
                the consumption and emission are plotted for.

        Raises:
            Exception: If the value given for prediction_periods
                is not of type int
            Exception: If the value given for prediction_periods
                is not greater of equal than 1.
        """
        df_energy = self.data

        if not isinstance(prediction_periods, int):
            raise Exception("prediction_periods is not of type int")

        if prediction_periods < 1:
            raise Exception("prediction_periods must be greater than or equal to 1")

        country_df = df_energy[df_energy["country"] == country]
        consumption_cols = [col for col in df_energy.columns if '_consumption' in col]

        info_predict = {
            "emissions": {"data": country_df["Emissions"].replace({0: np.NaN}).dropna()
                          , "units": "Tonnes CO2"},
            "consumption": {"data": country_df[consumption_cols]
                            .sum(axis=1).replace({0: np.NaN}).dropna(),"units": "TWh"}
        }

        plot,axes = plt.subplots(1, len(info_predict), figsize=(12, 4))

        for graph, prediction in enumerate(info_predict):
            data = info_predict[prediction]["data"]
            model = ARIMA(data, order=(5, 1, 1))
            model_fit = model.fit()
            forecast_data = model_fit.predict(len(data), len(data) + prediction_periods - 1).values
            forecast_index = (np.arange(0, prediction_periods) + 2019 + 1)
            forecast = pd.Series(data=forecast_data, index=forecast_index)
            ax1 = axes[graph]

            data.plot(ax=ax1, color="blue", label="Actual")
            forecast.plot(ax=ax1, color="red", label="Prediction")
            ax1.set_title(prediction.title())
            ax1.set_xlabel("Year")
            ax1.set_ylabel(f"{prediction.title()} in {info_predict[prediction]['units']}")

        plt.tight_layout()
        plt.legend()

    def emissions_consumption_scatter(self, year: int) -> None:
        """
        Creates a scatter plot between emissions and consumption for all countries, with the
        size of the dots should being the population.

        Parameters:
            countries (str/list): The country or countries to plot the emissions and
            consumptions for.

        Raises:
            ValueError: If one of the countries given as a parameter in countries is not in the
            dataset.
        """

        energy_df = self.data
        df_year= energy_df.loc[[year]]
        consumption_cols = [col for col in df_year.columns if "_consumption" in col]
        df_year = df_year.copy()
        df_year["total_consumption"] = df_year[consumption_cols].sum(axis=1)
        df_year = df_year.loc[df_year["total_consumption"] != 0].dropna(subset=["Emissions",
                                                                        "total_consumption"])
        df_year = df_year[df_year.country != "World"]

        plt.figure(figsize = (15,10))
        sns.scatterplot(data=df_year, x="Emissions", y="total_consumption",
                            size="population",hue="country",
                            legend=False, sizes=(20, 2000))
        plt.title("\nEmissions vs. Consumptions\n", fontsize=14)
        plt.ylabel("Consumption in TWh")
        plt.xlabel("Emissions")
        plt.ticklabel_format(style="plain", axis="x")
        plt.show()

